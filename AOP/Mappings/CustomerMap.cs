﻿using AOP.Entities;
using FluentNHibernate.Mapping;

namespace AOP.Mappings
{
    public class CustomerMap : ClassMap<Customer>
    {
        public CustomerMap()
        {
            Id(x => x.Id).GeneratedBy.GuidComb();
            Map(x => x.Name);
            Map(x => x.Surname);
            Map(x => x.OrderNo);
        }
    }
}
