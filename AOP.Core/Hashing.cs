﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace AOP
{
    static class Hashing
    {
        const int HashMultiplier = 33;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int ComputeHash(int value1, int value2) => unchecked(HashMultiplier * ComputeHash(value1) + value2);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int ComputeHash(int value) => value;

        public static int ComputeHash(IEnumerable<int> values)
        {
            unchecked
            {
                var hash = 0;

                foreach (var value in values)
                {
                    hash = HashMultiplier * hash + value;
                }

                return hash;
            }
        }
    }
}
