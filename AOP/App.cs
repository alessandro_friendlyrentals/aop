﻿using System;
using AOP.Data;
using AOP.Services;

namespace AOP
{
    public class App : IApp
    {
        readonly IDataContext _dataContext;
        readonly IService _service;

        public App(IDataContext dataContext, IService service)
        {
            _dataContext = dataContext;
            _service = service;
        }

        public void Run()
        {
            Console.WriteLine("App -> Run");

            _service.Save("hello");

            Console.WriteLine("Create 50 customers...");
            Console.ReadKey(true);

            _service.CreateCustomers(50);

            Console.WriteLine("Get first 3 customers...");
            Console.ReadKey(true);

            var customers = _service.GetCustomers(3);

            foreach (var customer in customers)
            {
                Console.WriteLine(customer.ToString());
            }

            Console.WriteLine("Get first 3 customers (cached)...");
            Console.ReadKey(true);

            customers = _service.GetCustomers(3);

            foreach (var customer in customers)
            {
                Console.WriteLine(customer.ToString());
            }

            Console.WriteLine("Get first 5 customers...");
            Console.ReadKey(true);

            customers = _service.GetCustomers(5);

            foreach (var customer in customers)
            {
                Console.WriteLine(customer.ToString());
            }

            Console.WriteLine("Get first 5 customers (cached)...");
            Console.ReadKey(true);

            customers = _service.GetCustomers(5);

            foreach (var customer in customers)
            {
                Console.WriteLine(customer.ToString());
            }

            Console.WriteLine("Go evil...");
            Console.ReadKey(true);

            try
            {
                _service.Evil();
            }
            catch { }

            Console.WriteLine("Get too much customers...");
            Console.ReadKey(true);

            try
            {
                _service.GetCustomers(101);
            }
            catch { }

            _service.Test(null);
        }
    }
}
