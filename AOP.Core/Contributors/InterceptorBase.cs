﻿using System;
using System.Diagnostics;
using System.Reflection;
using Castle.Core.Internal;
using Castle.DynamicProxy;

namespace AOP.Contributors
{
    public abstract class InterceptorBase<TAttribute> : IInterceptor where TAttribute : Attribute
    {
        public void Intercept(IInvocation invocation)
        {
#if DEBUG
            Debug.WriteLine($"{nameof(TAttribute)} : {typeof(TAttribute)}");
#endif

            var method = invocation.MethodInvocationTarget;
            var attribute = method.GetAttribute<TAttribute>();

            if (attribute == null)
            {
                invocation.Proceed();

                return;
            }

            InterceptInternal(invocation, method, attribute);
        }

        protected abstract void InterceptInternal(IInvocation invocation, MethodInfo method, TAttribute attribute);
    }
}
