﻿using System;
using System.Linq;
using AOP.Data;
using AOP.Entities;
using Castle.Core;
using NHibernate;
using NHibernate.Context;
using NHibernate.Linq;

namespace AOP.Services
{
    [CastleComponent("Service", typeof(IService), Lifestyle = LifestyleType.Transient)]
    public class Service : IService
    {
        readonly ISession _session;

        public Service(IDataContext context)
        {
            if (CurrentSessionContext.HasBind(context.Factory))
            {
                _session = context.Factory.GetCurrentSession();
            }
            else
            {
                _session = context.Factory.OpenSession();
                CurrentSessionContext.Bind(_session);
            }
        }

        [Logging]
        public void Save(object o) { }

        [Guard]
        [Transactional]
        [Logging]
        public void CreateCustomers(
            [MoreThan(Value = 100)]int count)
        {
            for (var i = 0; i < count; i++)
            {
                var customer = new Customer
                {
                    Name = "Name#" + i,
                    Surname = "Surname#" + i,
                    OrderNo = i
                };

                _session.Save(customer);
            }
        }

        [Guard]
        [Transactional]
        [Cache]
        [Logging]
        public IQueryable<Customer> GetCustomers([MoreThan(Value = 100)]int count) => _session.Query<Customer>().Take(count);

        [Logging]
        public void Evil() => throw new InvalidOperationException();

        [Guard]
        [Logging]
        public void Test([NotNull]object parameter) => Console.WriteLine(parameter.ToString());
    }
}
