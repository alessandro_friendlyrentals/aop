﻿using System.Linq;
using System.Reflection;

namespace AOP
{
    static class MethodSignature
    {
        public static int ComputeMethodHash(MethodInfo method, object[] arguments)
        {
            var values = arguments.Where(x => x != null).Select(x => x.GetHashCode());
            var hashes = Hashing.ComputeHash(values);

            return Hashing.ComputeHash(method.Name.GetHashCode(), hashes);
        }
    }
}
