﻿namespace AOP.Contributors
{
    public class LoggingContributor : ContributorBase<LoggingAttribute, LoggingInterceptor> { }
}
