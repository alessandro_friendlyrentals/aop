﻿using System;

namespace AOP
{
    static class Program
    {
        static void Main(string[] args)
        {
            Bootstrapper.Initialize();

            var app = Bootstrapper.Container.Resolve<IApp>();

            try
            {
                app.Run();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                Bootstrapper.Container.Release(app);
            }

            Console.ReadKey();
        }

        //static void Act(IDataContext context, IService srv)
        //{
        //    srv.Save("hello");
        //    srv.CreateCustomers(5);

        //    var customers = srv.GetCustomers(5);

        //    foreach (var customer in customers)
        //    {
        //        Console.WriteLine(customer.ToString());
        //    }

        //    try
        //    {
        //        srv.Evil();
        //    }
        //    catch { }

        //    try
        //    {
        //        srv.GetCustomers(6);
        //    }
        //    catch { }

        //    srv.Test(null);
        //}
    }
}
