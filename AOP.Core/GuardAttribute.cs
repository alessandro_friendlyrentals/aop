﻿using System;

namespace AOP
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class GuardAttribute : Attribute { }
}
