﻿using System;
using System.Reflection;
using Castle.Core;
using Castle.Core.Internal;
using Castle.DynamicProxy;

namespace AOP.Contributors
{
    [CastleComponent(nameof(GuardInterceptor), typeof(GuardInterceptor), Lifestyle = LifestyleType.Transient)]
    public class GuardInterceptor : InterceptorBase<GuardAttribute>
    {
        protected override void InterceptInternal(
            IInvocation invocation,
            MethodInfo method,
            GuardAttribute attribute)
        {
            var parameters = method.GetParameters();

            foreach (var parameter in parameters)
            {
                var notNull = parameter.GetAttribute<NotNullAttribute>();

                if (notNull != null)
                {
                    if (invocation.Arguments[parameter.Position] == null)
                    {
                        throw new ArgumentNullException(parameter.Name);
                    }
                }

                var lessThan = parameter.GetAttribute<MoreThanAttribute>();

                if (lessThan != null)
                {
                    var arg = invocation.Arguments[parameter.Position];

                    if (arg is IComparable comparableArg &&
                        comparableArg.CompareTo(lessThan.Value) > -1)
                    {
                        throw new ArgumentOutOfRangeException(parameter.Name);
                    }
                }
            }

            invocation.Proceed();
        }
    }
}
