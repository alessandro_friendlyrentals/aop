﻿using System;

namespace AOP
{
    [AttributeUsage(AttributeTargets.Parameter)]
    public sealed class NotNullAttribute : Attribute { }
}
