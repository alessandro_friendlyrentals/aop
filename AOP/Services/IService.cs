﻿using System.Linq;
using AOP.Entities;

namespace AOP.Services
{
    public interface IService
    {
        void Save(object o);

        void CreateCustomers(int count);

        IQueryable<Customer> GetCustomers(int count);

        void Evil();

        void Test(object parameter);
    }
}
