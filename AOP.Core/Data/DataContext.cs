﻿using System.Reflection;
using FluentNHibernate.Cfg;
using NHibernate;
using NHibernate.Context;
using NHibernate.Tool.hbm2ddl;

namespace AOP.Data
{
    public sealed class DataContext : IDataContext
    {
        bool _disposed;

        public ISessionFactory Factory { get; private set; }

        /// <summary>
        /// Code first
        /// </summary>
        public DataContext(Assembly assembly)
        {
            var cfg = new global::NHibernate.Cfg.Configuration().Configure("nhibernate-sqlite.config");
            //var assembly = Assembly.GetExecutingAssembly();
            var config = Fluently
                .Configure(cfg)
                .Mappings(x => x.FluentMappings.AddFromAssembly(assembly))
                .CurrentSessionContext<ThreadStaticSessionContext>()
                .BuildConfiguration();

            new SchemaUpdate(config).Execute(true, true);

            Factory = config.BuildSessionFactory();
        }

        public void Dispose()
        {
            if (!_disposed)
            {
                Factory.Dispose();

                _disposed = true;
            }
        }
    }
}
