﻿using System;

namespace AOP
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class CacheAttribute : Attribute { }
}
