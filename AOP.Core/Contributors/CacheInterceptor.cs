﻿using System.Collections.Concurrent;
using System.Reflection;
using Castle.Core;
using Castle.DynamicProxy;

namespace AOP.Contributors
{
    [CastleComponent(nameof(CacheInterceptor), typeof(GuardInterceptor), Lifestyle = LifestyleType.Transient)]
    public class CacheInterceptor : InterceptorBase<CacheAttribute>
    {
        static readonly ConcurrentDictionary<int, object> _cache = new ConcurrentDictionary<int, object>();

        protected override void InterceptInternal(
            IInvocation invocation,
            MethodInfo method,
            CacheAttribute attribute)
        {
            if (method.ReturnType == typeof(void))
            {
                return;
            }

            var key = MethodSignature.ComputeMethodHash(method, invocation.Arguments);

            if (_cache.ContainsKey(key))
            {
                invocation.ReturnValue = _cache[key];
            }
            else
            {
                invocation.Proceed();

                _cache[key] = invocation.ReturnValue;
            }
        }
    }
}
