﻿using System;

namespace AOP.Data
{
    public abstract class Entity
    {
        public virtual Guid Id { get; private set; }

        public virtual bool IsTransient => Id == Guid.Empty;
    }
}
