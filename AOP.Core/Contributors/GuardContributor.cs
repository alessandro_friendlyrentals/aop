﻿namespace AOP.Contributors
{
    public class GuardContributor : ContributorBase<GuardAttribute, GuardInterceptor> { }
}
