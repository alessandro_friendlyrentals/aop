using System;
using System.Linq;

namespace AOP.Data
{
    public interface IRepository<T> where T : Entity
    {
        T Get(Guid id);

        void Delete(T entity);

        void Save(T entity);

        IQueryable<T> Query();
    }
}
