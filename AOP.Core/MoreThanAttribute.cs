﻿using System;

namespace AOP
{
    [AttributeUsage(AttributeTargets.Parameter)]
    public sealed class MoreThanAttribute : Attribute
    {
        public int Value { get; set; }
    }
}
