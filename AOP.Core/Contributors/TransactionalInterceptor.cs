﻿using AOP.Data;
using Castle.Core;
using Castle.DynamicProxy;

namespace AOP.Contributors
{
    [CastleComponent(nameof(TransactionalInterceptor), typeof(TransactionalInterceptor), Lifestyle = LifestyleType.Transient)]
    public class TransactionalInterceptor : InterceptorBase<TransactionalAttribute>
    {
        readonly IDataContext _context;

        public TransactionalInterceptor(IDataContext context)
        {
            _context = context;
        }

        protected override void InterceptInternal(IInvocation invocation, System.Reflection.MethodInfo method, TransactionalAttribute attribute)
        {
            using (var trans = _context.Factory.GetCurrentSession().BeginTransaction())
            {
                try
                {
                    invocation.Proceed();
                    trans.Commit();
                }
                catch
                {
                    trans.Rollback();

                    throw;
                }
            }
        }
    }
}
