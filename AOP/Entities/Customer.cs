﻿using System.Globalization;
using AOP.Data;

namespace AOP.Entities
{
    public class Customer : Entity
    {
        public virtual string Name { get; set; }

        public virtual string Surname { get; set; }

        public virtual int OrderNo { get; set; }

        public override string ToString() => string.Format(CultureInfo.InvariantCulture, "{0} {1} [{2}]", Name, Surname, OrderNo);
    }
}
