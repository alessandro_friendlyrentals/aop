﻿using System.IO;
using System.Reflection;
using AOP.Contributors;
using AOP.Data;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using log4net;
using log4net.Config;

namespace AOP
{
    public class AOPInstaller : IWindsorInstaller
    {
        readonly Assembly _assembly;
        readonly string _configFile;

        public AOPInstaller(Assembly assembly, string configFile)
        {
            _assembly = assembly;
            _configFile = configFile;
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container
                .Kernel
                .ComponentModelBuilder
                .AddContributor(new LoggingContributor());
            container
                .Kernel
                .ComponentModelBuilder
                .AddContributor(new GuardContributor());
            container
                .Kernel
                .ComponentModelBuilder
                .AddContributor(new TransactionalContributor());

            var classes = Classes.FromThisAssembly();
            var autoRegistration = classes.Pick().If(Component.IsCastleComponent);
            var dataContextRegistration = new ComponentRegistration<IDataContext>()
                .UsingFactoryMethod(x => new DataContext(_assembly))
                .LifestyleSingleton();

            XmlConfigurator.Configure(new FileInfo(_configFile));

            var log4netRegistration = new ComponentRegistration<ILog>()
                .UsingFactoryMethod(x => LogManager.GetLogger(""))
                .LifestyleSingleton();

            container
                .Register(autoRegistration)
                .Register(dataContextRegistration)
                .Register(log4netRegistration);
        }
    }
}
