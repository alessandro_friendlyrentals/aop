﻿using System;
using System.Linq;
using AOP.Data;
using AOP.Entities;
using NHibernate;
using NHibernate.Context;
using NHibernate.Linq;

namespace AOP.Services
{
    public class Service_OldWay : IService
    {
        readonly ILog _log;
        readonly ISession _session;

        public Service_OldWay(
            ILog log, // we have to define dependency
            IDataContext context)
        {
            _log = log;

            if (CurrentSessionContext.HasBind(context.Factory))
            {
                _session = context.Factory.GetCurrentSession();
            }
            else
            {
                _session = context.Factory.OpenSession();
                CurrentSessionContext.Bind(_session);
            }
        }

        public void Save(object o)
        {
            try
            {
                _log.Write("");
            }
            catch (Exception ex)
            {
                _log.Write(ex.ToString());

                throw;
            }
        }

        public void CreateCustomers(int count)
        {
            if (count > 100)
            {
                throw new ArgumentOutOfRangeException();
            }

            try
            {
                _log.Write("");

                using (var transaction = _session.BeginTransaction())
                {
                    try
                    {
                        for (var i = 0; i < count; i++)
                        {
                            var customer = new Customer
                            {
                                Name = "Name#" + i,
                                Surname = "Surname#" + i,
                                OrderNo = i
                            };

                            _session.Save(customer);
                        }

                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();

                        throw;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Write(ex.ToString());

                throw;
            }
        }

        public IQueryable<Customer> GetCustomers(int count)
        {
            if (count > 100)
            {
                throw new ArgumentOutOfRangeException();
            }

            try
            {
                _log.Write("");

                using (var transaction = _session.BeginTransaction())
                {
                    try
                    {
                        var q = _session.Query<Customer>().Take(count);

                        transaction.Commit();

                        return q;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        _log.Write(ex.ToString());

                        throw;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Write(ex.ToString());

                throw;
            }
        }

        public void Evil()
        {
            try
            {
                throw new InvalidOperationException();
            }
            catch (Exception ex)
            {
                _log.Write(ex.ToString());

                throw;
            }
        }

        public void Test(object parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }

            Console.WriteLine(parameter.ToString());
        }
    }

    public interface ILog
    {
        void Write(string message);
    }
}
