﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;

namespace AOP
{
    public static class Bootstrapper
    {
        public static readonly WindsorContainer Container = new WindsorContainer();

        public static void Initialize()
        {
            var assembly = typeof(Bootstrapper).Assembly;
            var installer = new AOPInstaller(assembly, "log4net.config");

            Container.Install(installer);

            var registrations = Classes
                .FromThisAssembly()
                .Pick()
                .If(Component.IsCastleComponent);

            Container.Register(registrations);
            Container.Register(Component.For<IApp>().ImplementedBy<App>().LifestyleTransient());
        }
    }
}
