﻿using System;
using System.Linq;
using Castle.Core;
using NHibernate;
using NHibernate.Linq;

namespace AOP.Data
{
    [CastleComponent("RepositoryT", typeof(IRepository<>), Lifestyle = LifestyleType.Transient)]
    public class Repository<T> : IRepository<T> where T : Entity
    {
        readonly ISession session;

        public Repository(ISession session) => this.session = session;

        public T Get(Guid id) => session.Get<T>(id);

        public void Delete(T entity) => session.Delete(entity);

        public void Save(T entity) => session.SaveOrUpdate(entity);

        public IQueryable<T> Query() => session.Query<T>();
    }
}
