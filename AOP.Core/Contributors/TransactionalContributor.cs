﻿namespace AOP.Contributors
{
    public class TransactionalContributor : ContributorBase<TransactionalAttribute, TransactionalInterceptor> { }
}
