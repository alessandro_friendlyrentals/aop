﻿using System;
using System.Reflection;
using System.Text;
using Castle.Core;
using Castle.DynamicProxy;
using log4net;

namespace AOP.Contributors
{
    [CastleComponent(nameof(LoggingInterceptor), typeof(LoggingInterceptor), Lifestyle = LifestyleType.Transient)]
    public class LoggingInterceptor : InterceptorBase<LoggingAttribute>
    {
        readonly ILog _log;

        public LoggingInterceptor(ILog log) => _log = log;

        protected override void InterceptInternal(
            IInvocation invocation,
            MethodInfo method,
            LoggingAttribute attribute)
        {
            var messageBuilder = new StringBuilder();

            messageBuilder
                .Append(method.Name)
                .Append(" ( ");

            var parameters = method.GetParameters();

            foreach (var parameter in parameters)
            {
                messageBuilder
                    .Append(parameter.Name)
                    .Append(" : ")
                    .Append(invocation.Arguments[parameter.Position]);
            }

            var returnTypeIsVoid = method.ReturnType == typeof(void);

            if (returnTypeIsVoid)
            {
                messageBuilder.Append(" ) ");
            }
            else
            {
                messageBuilder.Append(" ) -> ");
            }

            try
            {
                invocation.Proceed();

                if (!returnTypeIsVoid)
                {
                    messageBuilder.AppendLine(invocation.ReturnValue?.ToString() ?? "null");
                }

                _log.Info(messageBuilder.ToString());
            }
            catch (Exception ex)
            {
                messageBuilder.AppendLine(ex.ToString());
                _log.Error(messageBuilder.ToString());

                throw;
            }
        }
    }
}
