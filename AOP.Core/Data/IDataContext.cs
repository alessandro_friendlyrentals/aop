﻿using System;
using NHibernate;

namespace AOP.Data
{
    public interface IDataContext : IDisposable
    {
        ISessionFactory Factory { get; }
    }
}
