﻿using System;
using System.Diagnostics;
using System.Linq;
using Castle.Core;
using Castle.Core.Internal;
using Castle.MicroKernel;
using Castle.MicroKernel.ModelBuilder;

namespace AOP.Contributors
{
    public abstract class ContributorBase<TAttribute, TInterceptor> : IContributeComponentModelConstruction
        where TAttribute : Attribute
        where TInterceptor : InterceptorBase<TAttribute>
    {
        public void ProcessModel(IKernel kernel, ComponentModel model)
        {
#if DEBUG
            Debug.WriteLine($"{nameof(TAttribute)} : {typeof(TAttribute)}");
            Debug.WriteLine($"{nameof(TInterceptor)} : {typeof(TInterceptor)}");
#endif
            var methods = model.Implementation.GetMethods();
            var methodsWithAttribute = methods.Where(method => AttributesUtil.GetAttribute<TAttribute>(method) != null);

            if (methodsWithAttribute.Any())
            {
                var interceptorReference = InterceptorReference.ForType<TInterceptor>();

                model.Interceptors.AddIfNotInCollection(interceptorReference);
            }
        }
    }
}
